`include "pack_str.sv"

module foo(output my_opcode_struct_t data);
my_opcode_struct_t cmd1;

initial begin
  cmd1.opcode = PREPARE;
  cmd1.target = FALCON1;
  cmd1.timestamp1 = 16'h0072;
  cmd1.timestamp2 = 16'h0023;


  
  case (cmd1.opcode) 
	  0:  $display("CMD OPCODE is INIT - 0");
	  1:  $display("CMD OPCODE is PREPARE - 1");
	  2:  $display("CMD OPCODE is LAUNCH - 2");
	  16'hffff:  $display("CMD OPCODE is ABORT - FFFF");
	
  endcase


  case (cmd1.target) 
	  1:  $display("TARGET is FALCON1");
	  9:  $display("TARGET is FALCON9");
  endcase

  $display("CMD TIMESTAMP1 is %d",cmd1.timestamp1);
  $display("CMD TIMESTAMP2 is %d",cmd1.timestamp2);
end

assign data = cmd1;

endmodule

// SIMPLE CACHE
`timescale 1 ns/100ps
module cache (input clk, input reset, 
	// find data
	input find, input [7:0] key, output reg match_found, output reg [7:0] value ,
	// update data
	input update, input [7:0] update_key, input [7:0] update_value
);
	localparam words = 8;
	logic [(words*16)-1:0] data_vec ;

	// find logic 
	always@(posedge clk) begin
		if (reset) begin
		    match_found <= 0;
		    value <= 0;
		end else begin 
		    match_found <= 0;
		    value <= 0;
		    for (int i = (words); i >0 ; i--) begin
		    	if (find) begin
		    		if (data_vec[(i*16)-1 -:8] == key) begin
		    			match_found <= 1;
		    			value <= data_vec[(i*16)-9 -:8];
		    		end
		    	end
		    end
		end
	end


	// update logic 
	always@(posedge clk) begin
		if (reset) begin
		    for (int i = 1; i <(words+1) ; i++) begin
		    	data_vec[(i*16)-1 -:8] <= i; // some default here
		    	data_vec[(i*16)-9 -:8] <= i+50; // some default here
		    end
		    $display("INIT-CACHE is %x",data_vec);
		end else begin
			if (update) begin
				data_vec[(words*16)-1 -:(words*16)] <= {data_vec[((words-1)*16)-1 -:((words-1)*16)], update_key, update_value};
			end
		end
	end

		    

endmodule 
/*

module tb ;
	logic find, match_found;
	logic [7:0] key, value;
	logic [7:0] update_key, update_value;
	logic update;
	logic clk, reset; 

	cache cache_1(.clk(clk), .reset(reset), .find(find), .key(key), .match_found(match_found), .value(value),
			.update(update), .update_key(update_key), .update_value(update_value)
			);

	initial begin
		reset <= 1;
		clk <= 0;
		#101 reset <= 0;
	end

	always @(clk) begin 
		#5 clk <= ~clk;
	end

	initial begin
		#121; 
		update_key <= 8'h01; update <= 1; update_value <= 100;
		#10  $display("UPD-CACHE is %x ",cache_1.data_vec);

		find <= 0;
		#10; 
		key = 8'h09; find = 1;
		#10  $display("Match for key 0x09 is %d value is %x",match_found, value);

		find <= 0;
		#10 key <= 8'h01; find <= 1;
		#10  $display("Match for key 0x01 is %d value is %x",match_found, value);

		$finish;
	end

endmodule 
*/

`timescale 1 ns/100ps

module tb();
parameter DWIDTH = 8;
parameter AWIDTH = 3;

reg areset_n;
reg wr_clk, rd_clk;
reg [DWIDTH-1:0] datain;
reg write;
reg read;
wire [DWIDTH-1:0] q;
wire empty, full;

dcfifo #(DWIDTH, AWIDTH) dcfifo_inst(
	.areset_n(areset_n), 
	.wr_clk(wr_clk), 
 	.datain(datain), 
	.write(write),

	.rd_clk(rd_clk), 
	.read(read), 
	.q(q),
	.empty(empty),
	.full(full)
);

initial begin
	areset_n <= 0;
	# 101 areset_n <= 1;
end

initial begin
	wr_clk <= 0;
	rd_clk <= 0;
end

always @(wr_clk) #10 wr_clk <= !wr_clk;
always @(rd_clk) #12 rd_clk <= !rd_clk;


initial 
begin
	datain = 0;
	write = 0;
	read = 0;
	#145

	datain = 8'h31;
	write = 1'b1;
	#20 write = 1'b0;
	#20

	datain = 8'h32;
	write = 1'b1;
	#20 write = 1'b0;
	#20

	datain = 8'h33;
	write = 1'b1;
	#20 write = 1'b0;
	#20

	datain = 8'h34;
	write = 1'b1;
	#20 write = 1'b0;
	#20


	datain = 8'h35;
	write = 1'b1;
	#20 write = 1'b0;
	#20


	datain = 8'h36;
	write = 1'b1;
	#20 write = 1'b0;
	#20


	datain = 8'h37;
	write = 1'b1;
	#20 write = 1'b0;
	#20

	//datain = 8'h38;
	//write = 1'b1;
	//#20 write = 1'b0;
	//#20



	read = 1'b1;
	#24 read = 1'b0;
	#24

	read = 1'b1;
	#24 read = 1'b0;
	#24

	read = 1'b1;
	#24 read = 1'b0;
	#24

	read = 1'b1;
	#24 read = 1'b0;
	#24

	read = 1'b1;
	#24 read = 1'b0;
	#24

	read = 1'b1;
	#24 read = 1'b0;
	#24

	read = 1'b1;
	#24 read = 1'b0;
	#24

	//read = 1'b1;
	//#24 read = 1'b0;
	//#24

	#50 $finish;

end



initial
 begin
    $dumpfile("dcfifo.vcd");
    $dumpvars(0,dcfifo_inst);
 end

endmodule // tb




module dcfifo(
	areset_n, 

	wr_clk, 
 	datain, 
	write,

	rd_clk,
	read, 
	q,

	empty,
	full
);
parameter DWIDTH=8;
parameter AWIDTH=6;
localparam  DEPTH=(1<<AWIDTH);

input wr_clk;
input areset_n;
input [DWIDTH-1:0] datain;
input write;

input rd_clk;
input read;
output [DWIDTH-1:0] q;
output full;
output empty;


wire [DWIDTH-1:0] q;
wire [DWIDTH-1:0] data_int;
reg full;
wire empty;
reg [AWIDTH-1:0] wptr, rptr;
reg [AWIDTH-1:0] wptr_ms, wptr_rd;
reg [AWIDTH-1:0] rptr_ms, rptr_wr;
reg [AWIDTH-1:0] wptr_next, rptr_wr_bin;
reg [AWIDTH-1:0] rptr_gray, wptr_gray;
reg [AWIDTH:0] fill;
integer i;

reg areset_n_ms, areset_n_wr;

always @(posedge wr_clk ) begin
	areset_n_ms <= areset_n;
	areset_n_wr <= areset_n_ms;
end

// create write section 
always @(posedge wr_clk or negedge areset_n_wr) begin
	if (!areset_n_wr) begin
		wptr <= #1 {AWIDTH{1'b0}};
	end else if (write) begin
		wptr <= #1 wptr+1;
	end
end
assign wptr_gray = (wptr >>1) ^ wptr;
assign wptr_next = wptr+1;
assign full  = (wptr_next == rptr_wr_bin);


always@(*)
   for (i=0; i<AWIDTH; i = i+1) begin
     rptr_wr_bin[i] = ^(rptr_wr >> i);
   end



always @(posedge wr_clk ) begin
	rptr_ms <= rptr_gray;
	rptr_wr <= rptr_ms;
end

always @(posedge rd_clk ) begin
	wptr_ms <= wptr_gray;
	wptr_rd <= wptr_ms;
end





// create read section 
always @(posedge rd_clk or negedge areset_n_wr) begin
	if (!areset_n_wr) begin
		rptr <= #1 {AWIDTH{1'b0}};
	end else if (read) begin
		rptr <= #1 rptr+1;;
	end
end



assign rptr_gray = (rptr>>1) ^ rptr;

assign q = data_int;


assign	empty = (rptr_gray == wptr_rd);


// instance of dual port RAM here 
dp_dcram #(DWIDTH, AWIDTH) dp_dcram_0(
	// write port
	.wr_clk(wr_clk),
	.addr0(wptr),
	.data0(datain),
	.we0(write),
	
	// read port 
	.rd_clk(rd_clk),
	.addr1(rptr),
	.q1(data_int),
	.re1(read)
);

endmodule // scfifo





module dp_dcram(
	wr_clk,
	addr0 ,
	data0,
	we0,

	rd_clk,
	addr1,
	q1,
	re1
);
parameter DWIDTH = 8;
parameter AWIDTH = 8;


input wr_clk;
input rd_clk;
input [DWIDTH-1:0] data0; 
output [DWIDTH-1:0] q1;
input [AWIDTH-1:0] addr0;
input [AWIDTH-1:0] addr1;
input we0, re1;

reg [DWIDTH-1:0] Mem [0:(1<<AWIDTH)-1];
reg [DWIDTH-1:0] q1;

always @(posedge wr_clk)
  if (we0)
    Mem[addr0] <= data0;

always @(posedge rd_clk)
  if (re1)
    q1 <=  Mem[addr1] ;

endmodule 


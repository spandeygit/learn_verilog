// Demo for loop in system verilog 
// Demo bit selects in system verilog 
//
module tb ;
	logic [31:0] data_vec = 32'h01020304;

	initial begin
		$display("Value of word is 0x%x",data_vec);
		for (int i = 1; i <5 ; i++) begin
			$display("Value at byte %d is 0x%x",i-1,data_vec[(i*8)-1 -:8]);
		end
	end
endmodule 

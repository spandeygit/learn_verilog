`timescale 1 ns/100ps


module tb();
parameter DWIDTH = 8;
parameter AWIDTH = 4;

reg reset_n;
reg clock;
reg [DWIDTH-1:0] datain;
reg write;
reg read;
wire [DWIDTH-1:0] q;

scfifo #(DWIDTH, AWIDTH) scfifo_inst(
	.reset_n(reset_n), 
	.clock(clock), 
 	.datain(datain), 
	.write(write),
	.read(read), 
	.q(q)
);

initial begin
	reset_n <= 0;
	# 101 reset_n <= 1;
end

initial begin
	clock <= 0;
end

always @(clock) #10 clock <= !clock;

initial 
begin
	datain = 0;
	write = 0;
	read = 0;
	#115 
	datain = 8'h35;
	write = 1'b1;
	#20 write = 1'b0;
	#20
	datain = 8'h45;
	write = 1'b1;
	#20 write = 1'b0;
	#20
	read = 1'b1;
	#20 read = 1'b0;
	#20
	read = 1'b1;
	#20 read = 1'b0;

	#50 $finish;

end

initial
 begin
    $dumpfile("scfifo.vcd");
    $dumpvars(0,scfifo_inst);
 end

endmodule // tb


module scfifo(
	reset_n, // synch reset 
	clock, // clock
 	datain, // n bit data
	write,
	read, 
	q
);
parameter DWIDTH=8;
parameter AWIDTH=6;
localparam  DEPTH=(1<<AWIDTH);

input clock;
input reset_n;
input [DWIDTH-1:0] datain;
input write;
input read;
output [DWIDTH-1:0] q;
output full;
output empty;


reg [DWIDTH-1:0] q;
wire [DWIDTH-1:0] data_int;
wire full;
wire empty;
reg [AWIDTH-1:0] wptr, rptr;
reg [AWIDTH:0] fill;

assign full = (fill == DEPTH-1);
assign empty = (fill == 0);


// create write section 
always @(posedge clock or negedge reset_n) begin
	if (!reset_n) begin
		wptr <= {AWIDTH{1'b0}};
	end else if (write) begin
		wptr <= wptr+1;;
	end
end

// create read section 
always @(posedge clock or negedge reset_n) begin
	if (!reset_n) begin
		rptr <= {AWIDTH{1'b0}};
	end else if (read) begin
		rptr <= rptr+1;;
	end
end

always @(posedge clock or negedge reset_n) begin
	if (!reset_n) begin
		q <= {DWIDTH{1'b0}};
	end else if (read) begin
		q <= data_int;	
	end
end

always @(posedge clock or negedge reset_n) begin
	if (!reset_n) begin
		fill <= {AWIDTH+1{1'b0}};
	end else if (read & !write &!(fill==0)) begin
		fill <= fill -1;	
	end else if (!read & write & !(fill == DEPTH-1)) begin
		fill <= fill +1;	
	end
end


// instance of dual port RAM here 
dp_ram #(DWIDTH, AWIDTH) dp_ram_0(
	// write port
	.addr0(wptr),
	.data0(datain),
	.we0(write),
	
	// read port 
	.addr1(rptr),
	.q1(data_int),
	.re1(read)
	
);

endmodule // scfifo


module dp_ram(
	addr0 ,
	data0,
	we0,

	addr1,
	q1,
	re1
);
parameter DWIDTH = 8;
parameter AWIDTH = 8;


input [DWIDTH-1:0] data0; 
output [DWIDTH-1:0] q1;
input [AWIDTH-1:0] addr0;
input [AWIDTH-1:0] addr1;
input we0, re0, we1,re1;

reg [DWIDTH-1:0] Mem [0:(1<<AWIDTH)-1];
assign q1 =  Mem[addr1] ;

always @(we0)
  if (we0)
    Mem[addr0] = data0;

endmodule 


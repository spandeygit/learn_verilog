`timescale 1ns / 1ps


/*
module pwm (
    input wire logic clk,
    input wire logic [7:0] duty,
    output     logic pwm_out
    );

    logic [7:0] cnt = 8'b0;
    always_ff @(posedge clk) begin
        cnt <= cnt + 1;
    end

    always_comb pwm_out = (cnt < duty);
endmodule

*/



module top(
    input  wire logic reset_n,
    input  wire logic clk,
    input  wire logic [3:0] sw,
    input  wire logic [3:0] btn,
    
    output      logic [3:0] led_r,
    output      logic [3:0] led_g,
    output      logic [3:0] led_b
    );

    localparam IDLE = 3'b000;
    localparam GOINGUP_NEXT = 3'b001;
    localparam GOINGUP = 3'b010;
    localparam GOINGDN_NEXT = 3'b011;
    localparam GOINGDN = 3'b100;
    
    // state machine in ARTIX A7-35T board
    // We will use BTN[0] --> ACT switch 
    // We will use BTN[1] --> Reached bottom
    reg activate ;
    reg button_d_act;
    reg bottom_reached;
    reg top_reached;
    reg button_d_br;
    reg button_d_tr;
    reg reset_n_d, reset_n_2d;
    reg [2:0] state, state_next;
    reg [3:0] led_r_next, led_g_next, led_b_next;
    //reg [3:0] led_r, led_g, led_b;    
    wire top_reached_db;
    wire bottom_reached_db;
    wire activate_db;
    reg activate_db_d, top_reached_db_d, bottom_reached_db_d;
    
    
    // synchronize the reset 
    always @(posedge clk) begin
        reset_n_d <= reset_n;
        reset_n_2d <= reset_n_d;   
        
        button_d_act <= btn[0];
        activate <= button_d_act;
        
        button_d_br <= btn[1];
        bottom_reached <= button_d_br;
        
        button_d_tr <= btn[2];
        top_reached <= button_d_tr;
        
    end

    always @(posedge clk, negedge reset_n_2d) begin
        if (!reset_n_2d) begin
            state <= IDLE;
            led_r <= 3'b000;
            led_g <= 3'b000;
            led_b <= 3'b000;
            activate_db_d <= 0;
            bottom_reached_db_d <= 0;
            top_reached_db_d <= 0;
        end else begin
            state <= state_next;
            led_r <= led_r_next;
            led_g <= led_g_next;
            led_b <= led_b_next;
            activate_db_d <= activate_db;
            bottom_reached_db_d <= bottom_reached_db;
            top_reached_db_d <= top_reached_db;
        end
    end 
    
  debounce deboune_1(.reset_n(reset_n_2d), .clk(clk), .sig(activate), .sig_debounce(activate_db)   );
  debounce deboune_2(.reset_n(reset_n_2d), .clk(clk), .sig(top_reached), .sig_debounce(top_reached_db)   );
  debounce deboune_3(.reset_n(reset_n_2d), .clk(clk), .sig(bottom_reached), .sig_debounce(bottom_reached_db)   );
    
  always_comb begin
        state_next = state;
        led_r_next = led_r;
        led_g_next = led_g;
        led_b_next = led_b;
        case (state)
            IDLE: begin
               
                state_next = GOINGUP_NEXT;
            end
            
            
            GOINGUP_NEXT : begin
                if (!activate_db && activate_db_d) begin
                    // activate detected 
                    led_r_next = 1'b0;
                    led_g_next = 1'b1;
                    led_b_next = 1'b0;
                    state_next = GOINGUP;
                end
            end
            
            
            GOINGUP : begin
                if (!activate_db && activate_db_d) begin
                    // activate detected 
                    led_r_next = 1'b0;
                    led_g_next = 1'b0;
                    led_b_next = 1'b1;
                    state_next = GOINGDN_NEXT;
                end
                
                if (!top_reached_db && top_reached_db_d) begin
                    // activate detected 
                    led_r_next = 1'b0;
                    led_g_next = 1'b0;
                    led_b_next = 1'b0;
                    state_next = GOINGDN_NEXT;
                end
                
            end
            
            GOINGDN_NEXT : begin
                if (!activate_db && activate_db_d) begin
                    // activate detected 
                    led_r_next = 1'b1;
                    led_g_next = 1'b0;
                    led_b_next = 1'b0;
                    state_next = GOINGDN;
                end
            end
            
            GOINGDN : begin
                if (!activate_db && activate_db_d) begin
                    // activate detected 
                    led_r_next = 1'b0;
                    led_g_next = 1'b0;
                    led_b_next = 1'b1;
                    state_next = GOINGUP_NEXT;
                end
                
                if (!bottom_reached_db && bottom_reached_db_d) begin
                    // activate detected 
                    led_r_next = 1'b0;
                    led_g_next = 1'b0;
                    led_b_next = 1'b0;
                    state_next = GOINGUP_NEXT;
                end
            end
            
             default : begin
                state_next = IDLE;
            end
            
        endcase
  
    end

endmodule


module debounce(
    input  wire logic reset_n,
    input  wire logic clk,
    input  wire logic sig,
    output logic sig_debounce
    );
    
  reg state, sig_d;
  wire diff;
  reg [5:0] count;
  assign diff = sig ^ sig_d;
  
  
  // assume sig = 1 as our signals are active low
  always @(posedge clk) begin
     if (reset_n==0) begin
        sig_debounce <= 1;
        sig_d <= 0;
        count <= 0;
     end else begin
        sig_d <= sig;
        if (diff) count <= 0;
        else begin
            if (count != 31) begin
                count <= count+1;
            end 
        end       
        
        if (count == 20) sig_debounce <= !sig_d; // latch sig_d state as it has been stable for 20 clocks
     end 
  end  
    
endmodule

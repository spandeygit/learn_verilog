typedef enum logic[15:0]
{
  LAUNCH = 16'h0002,
  ABORT = 16'hFFFF,
  INIT = 16'h0000,
  PREPARE = 16'h0001
} my_opcode_t;

typedef enum logic[15:0]
{
  FALCON1 = 16'h0001,
  FALCON9 = 16'h0009
} my_target_t;

typedef struct packed
{
  my_opcode_t  opcode; // 16-bit opcode, enumerated type
  my_target_t  target; // 16-bit target, enumerated type
  logic [15:0] timestamp1;
  logic [15:0] timestamp2;
} my_opcode_struct_t;

